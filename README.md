# newfastpizza-frontend-application

1. Download the [latest nodejs](https://nodejs.org/en/download/) if you don't have it.
2. Make sure the **npm** package manager is working. You can try to run `npm -v` command in cmd.

## Run the app in dev mode

From root project folder run `npm install` and `npm start` commands. The app should start at `http://localhost:3000`.

## Run the build

This option is suitable for those who do not want to download all the necessary dependencies to run app in dev mode.

1. Run `npm install -g serve` to globaly install package that can serve static site.
2. Run `serve -s <path to project build folder>` to serve the app. In the cmd you will see information about the serving app and the address on which it is serving.

# Note

* Functionality for admin you can find here `<root app address>/admin`.
* The app requires a backend server running on port 8080. You can set a different port value in the first line of `root\src\core\http.ts` file (only for running in dev mode).
