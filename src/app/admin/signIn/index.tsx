import { unwrapResult } from '@reduxjs/toolkit';
import React, { useCallback, useState } from 'react';
import { Modal } from 'react-bootstrap';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';

import { TextBoxField } from '../../../common/fields/textBoxField';
import { LoadingButton } from '../../../common/parts/loadingButton';
import { useAppDispatch } from '../../../core/store';
import { SignInRequest } from '../../../data/auth/models';
import { signIn } from '../../../data/auth/slice';

const schema = yup.object().shape({
  username: yup.string().required().label('Username'),
  password: yup.string().required().label('Password'),
});

export const SignIn: React.FC = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();

  const [form, setForm] = useState<SignInRequest>({
    username: '',
    password: '',
  });

  const onChange = useCallback((field: keyof SignInRequest, value: string) => {
    setForm((form) => ({ ...form, [field]: value }));
  }, []);

  const [validate, setValidate] = useState(false);

  const onSignInClick = useCallback(() => {
    schema
      .validate(form)
      .then(() =>
        dispatch(signIn(form))
          .then(unwrapResult)
          .then(() => history.push('/admin'))
      )
      .catch(() => setValidate(true));
  }, [dispatch, form, history]);

  return (
    <Modal show animation={false} centered size="sm">
      <Modal.Header closeButton={false}>
        <Modal.Title>Admin</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <TextBoxField
          controlId="username"
          value={form.username}
          onChange={(v) => onChange('username', v)}
          validate={validate}
          schema={schema}
          fieldPath="username">
          Username
        </TextBoxField>
        <TextBoxField
          controlId="password"
          value={form.password}
          onChange={(v) => onChange('password', v)}
          type="password"
          validate={validate}
          schema={schema}
          fieldPath="password">
          Password
        </TextBoxField>
      </Modal.Body>
      <Modal.Footer>
        <LoadingButton asyncThunks={[signIn]} onClick={onSignInClick} className="mr-auto">
          Sign in
        </LoadingButton>
      </Modal.Footer>
    </Modal>
  );
};
