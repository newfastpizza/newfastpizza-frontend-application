import React from 'react';
import { useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { CheckBox } from '../../../../../common/fields/checkbox';
import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { useCleaning } from '../../../../../core/customHooks/useCleaning';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { getAllTownPoints, resetTownPoints, setOfficeStatus } from '../../../../../data/town/slice';

export const TownPointsTable: React.FC = () => {
  useCleaning(resetTownPoints);

  const dispatch = useAppDispatch();
  const points = useSelector((store: RootState) => store.town.points);

  useEffect(() => {
    dispatch(getAllTownPoints());
  }, [dispatch]);

  return (
    <LoadingPanel asyncThunks={[getAllTownPoints]}>
      <Table hover>
        <thead>
          <tr>
            <th>Street</th>
            <th>Building</th>
            <th>Is branch office</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {points?.map((x) => (
            <tr key={x.id}>
              <td>{x.street}</td>
              <td>{x.building}</td>
              <td>
                <CheckBox
                  name="isBranchOffice"
                  value={x.isBranchOffice}
                  onChange={(v) => dispatch(setOfficeStatus({ id: x.id, value: v }))}
                />
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </LoadingPanel>
  );
};
