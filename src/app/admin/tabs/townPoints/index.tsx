import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { TownPointsTable } from './table';

export const TownPointsTabIndex: React.FC = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={match.url} component={TownPointsTable} />
      <Redirect to={match.url} />
    </Switch>
  );
};
