import { unwrapResult } from '@reduxjs/toolkit';
import React, { useCallback, useEffect, useState } from 'react';
import { Table } from 'react-bootstrap';
import { Button, Col, Form, Modal } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { TextAreaField } from '../../../../../common/fields/textAreaField';
import { TextBoxField } from '../../../../../common/fields/textBoxField';
import { DeleteModal } from '../../../../../common/parts/deleteModal';
import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { useCleaning } from '../../../../../core/customHooks/useCleaning';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { showAlert } from '../../../../../data/alerts/slice';
import {
  deleteOrderById,
  deliverOrder,
  finishOrder,
  getClientForOrderById,
  getDriverForOrderById,
  getOrderById,
  resetOrder,
} from '../../../../../data/orders/slice';
import { TownPoint } from '../../../../../data/town/models';

const getAddress = (townPoint?: TownPoint | null) => {
  return townPoint ? `${townPoint.street} ${townPoint.building}` : null;
};

interface Props {
  id: string;
  onHide: () => void;
}

export const DetailsModal: React.FC<Props> = ({ id, onHide }) => {
  useCleaning(resetOrder);
  const dispatch = useAppDispatch();

  const { order, client, driver } = useSelector((state: RootState) => state.orders.item);

  useEffect(() => {
    dispatch(getOrderById(id));
    dispatch(getClientForOrderById(id));
    dispatch(getDriverForOrderById(id));
  }, [dispatch, id]);

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const onDelete = useCallback(() => {
    dispatch(deleteOrderById(id))
      .then(unwrapResult)
      .then(() => {
        dispatch(showAlert({ message: 'Order has been deleted', type: 'warning' }));
        onHide();
      });
  }, [dispatch, onHide, id]);

  const onDeliverClick = useCallback(() => {
    dispatch(deliverOrder(id))
      .then(unwrapResult)
      .then(() => {
        dispatch(showAlert({ message: 'Order status changed successfully' }));
      });
  }, [dispatch, id]);

  const onFinishClick = useCallback(() => {
    dispatch(finishOrder(id))
      .then(unwrapResult)
      .then(() => {
        dispatch(showAlert({ message: 'Order status changed successfully' }));
      });
  }, [dispatch, id]);

  const date = order?.date ? new Date(order?.date) : null;

  return (
    <LoadingPanel asyncThunks={[getOrderById, getClientForOrderById, getDriverForOrderById]}>
      <Modal show={!showDeleteModal} onHide={onHide} animation={false} centered>
        <Modal.Header closeButton>
          <Modal.Title>Order details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Row>
            <Col>
              <TextBoxField controlId="client" value={client?.name} disabled>
                Client
              </TextBoxField>
            </Col>
            <Col>
              <TextBoxField controlId="phone" value={order?.phone} disabled>
                Phone
              </TextBoxField>
            </Col>
          </Form.Row>
          <Form.Row>
            <Col>
              <TextBoxField controlId="address" value={getAddress(order?.townPoint)} disabled>
                Address
              </TextBoxField>
            </Col>
            <Col>
              <TextBoxField controlId="flatNumber" value={order?.flatNumber} disabled>
                Flat number
              </TextBoxField>
            </Col>
          </Form.Row>
          <Form.Row>
            <Col>
              <TextBoxField
                controlId="date"
                value={date ? `${date.toLocaleDateString()} ${date.toLocaleTimeString()}` : null}
                disabled>
                Date
              </TextBoxField>
            </Col>
            <Col>
              <TextAreaField controlId="comment" value={order?.comment} disabled>
                Comment
              </TextAreaField>
            </Col>
          </Form.Row>
          <Form.Row>
            <Col>
              <TextBoxField controlId="status" value={order?.status} disabled>
                Status
              </TextBoxField>
            </Col>
            <Col>
              <TextBoxField controlId="driver" value={driver?.name} disabled>
                Driver
              </TextBoxField>
            </Col>
          </Form.Row>
          <Table hover>
            <thead>
              <tr>
                <th>Product</th>
                <th>Count</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {order?.backet?.map((x) => (
                <tr key={x.id}>
                  <td>{x.product.name}</td>
                  <td>{x.count}</td>
                  <td>${x.price}</td>
                </tr>
              ))}
              <tr className="table-success">
                <td>Total</td>
                <td>{order?.backet?.reduce((p, c) => p + c.count, 0)}</td>
                <td>${order?.backet?.reduce((p, c) => p + c.price, 0)}</td>
              </tr>
            </tbody>
          </Table>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setShowDeleteModal(true)} className="mr-auto" variant="danger" hidden>
            Delete
          </Button>
          {order?.status === 'IS_PREPARED' && driver && <Button onClick={onDeliverClick}>Deliver</Button>}
          {order?.status === 'IS_DELIVERED' && <Button onClick={onFinishClick}>Finish</Button>}
        </Modal.Footer>
      </Modal>
      {showDeleteModal && (
        <DeleteModal
          title="Delete order"
          asyncThunk={deleteOrderById}
          onHide={() => setShowDeleteModal(false)}
          onDelete={onDelete}
        />
      )}
    </LoadingPanel>
  );
};
