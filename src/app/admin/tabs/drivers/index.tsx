import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { DriversTable } from './table';

export const DriversTabIndex: React.FC = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={match.url} component={DriversTable} />
      <Redirect to={match.url} />
    </Switch>
  );
};
