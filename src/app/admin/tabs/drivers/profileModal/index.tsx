import { unwrapResult } from '@reduxjs/toolkit';
import React, { useCallback, useEffect, useState } from 'react';
import { Button, Col, Form, Modal } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import * as yup from 'yup';

import { TextBoxField } from '../../../../../common/fields/textBoxField';
import { DeleteModal } from '../../../../../common/parts/deleteModal';
import { LoadingButton } from '../../../../../common/parts/loadingButton';
import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { showAlert } from '../../../../../data/alerts/slice';
import { ChangeDriverProfileRequest } from '../../../../../data/drivers/models';
import {
  deleteDriver,
  getDriverById,
  getOrderForDriverByid,
  getPositionForDriverById,
  updateDriverInfo,
} from '../../../../../data/drivers/slice';
import { TownPoint } from '../../../../../data/town/models';
import { nameRegex, phoneRegex } from '../../../../../utils/regex';

const getAddress = (townPoint?: TownPoint | null) => {
  return townPoint ? `${townPoint.street} ${townPoint.building}` : null;
};

const driverInfoSchema = yup.object().shape({
  name: yup
    .string()
    .required()
    .test({
      test: (v: string) => nameRegex.test(v),
      message: 'Name can consist of uppercase and lowercase latin letters or spaces',
    } as yup.TestConfig)
    .label('Name'),
  phone: yup
    .string()
    .required()
    .test({
      test: (v: string) => phoneRegex.test(v),
      message: 'Phone should have phone format',
    } as yup.TestConfig)
    .label('Phone'),
});

interface Props {
  id: string;
  onHide: () => void;
}

export const ProfileModal: React.FC<Props> = ({ id, onHide }) => {
  const dispatch = useAppDispatch();

  const { driver, order, position } = useSelector((store: RootState) => store.drivers.item);

  const [driverInfo, setDriverInfo] = useState<ChangeDriverProfileRequest>({
    name: '',
    phone: '',
  });

  const isDriverInfoChanges = driver && (driver.name !== driverInfo.name || driver.phone !== driverInfo.phone);

  useEffect(() => {
    dispatch(getDriverById(id))
      .then(unwrapResult)
      .then((x) => setDriverInfo({ name: x.name, phone: x.phone }));
    dispatch(getOrderForDriverByid(id));
    dispatch(getPositionForDriverById(id));
  }, [dispatch, id]);

  const [validateDriverInfo, setValidateDriverInfo] = useState(false);

  const onChangeDriverInfo = useCallback((field: keyof ChangeDriverProfileRequest, value: string) => {
    setDriverInfo((form) => ({ ...form, [field]: value }));
  }, []);

  const onUpdateProfileClick = useCallback(() => {
    driverInfoSchema
      .validate(driverInfo)
      .then(() =>
        dispatch(updateDriverInfo({ id, model: driverInfo }))
          .then(unwrapResult)
          .then(() => {
            dispatch(showAlert({ message: "Driver's profile has been successfully updated" }));
          })
      )
      .catch(() => setValidateDriverInfo(true));
  }, [driverInfo, dispatch, id]);

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const onDelete = useCallback(() => {
    dispatch(deleteDriver(id))
      .then(unwrapResult)
      .then(() => {
        dispatch(showAlert({ message: "Driver's profile  has been deleted", type: 'warning' }));
        onHide();
      });
  }, [dispatch, id, onHide]);

  return (
    <LoadingPanel asyncThunks={[getDriverById, getOrderForDriverByid, getPositionForDriverById]}>
      <Modal show={!showDeleteModal} onHide={onHide} animation={false} centered>
        <Modal.Header closeButton>
          <Modal.Title>Driver profile</Modal.Title>
        </Modal.Header>
        <LoadingPanel asyncThunks={[getDriverById]}>
          <Modal.Body>
            <Form.Row>
              <Col>
                <TextBoxField controlId="username" value={driver?.username} disabled>
                  Username
                </TextBoxField>
              </Col>
              <Col>
                <TextBoxField
                  controlId="name"
                  value={driverInfo.name}
                  onChange={(v) => onChangeDriverInfo('name', v)}
                  validate={validateDriverInfo}
                  schema={driverInfoSchema}
                  fieldPath="name">
                  Name
                </TextBoxField>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col>
                <TextBoxField
                  controlId="phone"
                  value={driverInfo.phone}
                  onChange={(v) => onChangeDriverInfo('phone', v)}
                  validate={validateDriverInfo}
                  schema={driverInfoSchema}
                  fieldPath="phone">
                  Phone
                </TextBoxField>
              </Col>
              <Col>
                <TextBoxField controlId="status" value={driver?.status} disabled>
                  Status
                </TextBoxField>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col>
                <TextBoxField controlId="order" value={order?.id} disabled>
                  Order id
                </TextBoxField>
              </Col>
              <Col>
                <TextBoxField controlId="curAddress" value={getAddress(position)} disabled>
                  Current address
                </TextBoxField>
              </Col>
            </Form.Row>
          </Modal.Body>
        </LoadingPanel>
        <Modal.Footer>
          <Button onClick={() => setShowDeleteModal(true)} className="mr-auto" variant="danger">
            Delete
          </Button>
          {isDriverInfoChanges && (
            <LoadingButton asyncThunks={[updateDriverInfo]} onClick={onUpdateProfileClick}>
              Update profile
            </LoadingButton>
          )}
        </Modal.Footer>
      </Modal>
      {showDeleteModal && (
        <DeleteModal
          title="Delete driver"
          asyncThunk={deleteDriver}
          onHide={() => setShowDeleteModal(false)}
          onDelete={onDelete}
        />
      )}
    </LoadingPanel>
  );
};
