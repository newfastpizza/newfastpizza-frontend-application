import React, { useState } from 'react';
import { useEffect } from 'react';
import { Button, Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { useCleaning } from '../../../../../core/customHooks/useCleaning';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { getDrivers, resetDrivers } from '../../../../../data/drivers/slice';
import { ProfileModal } from '../profileModal';
import { SignUpDriverModal } from '../signUpDriverModal';

export const DriversTable: React.FC = () => {
  useCleaning(resetDrivers);

  const dispatch = useAppDispatch();
  const drivers = useSelector((store: RootState) => store.drivers.items);

  useEffect(() => {
    dispatch(getDrivers());
  }, [dispatch]);

  const [showProfileId, setShowProfileId] = useState<string | null>(null);
  const [showSignUpDriverModal, setShowSignUpDriverModal] = useState(false);

  return (
    <LoadingPanel asyncThunks={[getDrivers]}>
      <Table hover>
        <thead>
          <tr>
            <th>Username</th>
            <th>Name</th>
            <th>Phone</th>
            <th>Status</th>
            <th>
              <Button onClick={() => setShowSignUpDriverModal(true)} variant="success" size="sm">
                Add
              </Button>
            </th>
          </tr>
        </thead>
        <tbody>
          {drivers?.map((x) => (
            <tr key={x.id}>
              <td>{x.username}</td>
              <td>{x.name}</td>
              <td>{x.phone}</td>
              <td>{x.status}</td>
              <td>
                <Button onClick={() => setShowProfileId(x.id)} variant="secondary" size="sm">
                  Profile
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {showProfileId && <ProfileModal id={showProfileId} onHide={() => setShowProfileId(null)} />}
      {showSignUpDriverModal && (
        <SignUpDriverModal onHide={() => setShowSignUpDriverModal(false)} onSignUp={() => dispatch(getDrivers())} />
      )}
    </LoadingPanel>
  );
};
