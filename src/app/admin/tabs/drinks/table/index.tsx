import React, { useState } from 'react';
import { useEffect } from 'react';
import { Button, Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { useCleaning } from '../../../../../core/customHooks/useCleaning';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { getAllDrinks, resetDrinks } from '../../../../../data/products/slice';
import { DetailsModal } from '../detailsModal';

export const DrinksTable: React.FC = () => {
  useCleaning(resetDrinks);

  const dispatch = useAppDispatch();
  const drinks = useSelector((store: RootState) => store.products.drinks);

  useEffect(() => {
    dispatch(getAllDrinks());
  }, [dispatch]);

  const [currentDrinkId, setCurrentDrinkId] = useState<string | null>(null);

  return (
    <LoadingPanel asyncThunks={[getAllDrinks]}>
      <Table hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Volume (L)</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {drinks?.map((x) => (
            <tr key={x.id}>
              <td>{x.name}</td>
              <td>{x.price}</td>
              <td>{x.volume}</td>
              <td>
                <Button onClick={() => setCurrentDrinkId(x.id)} variant="secondary" size="sm">
                  Details
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {currentDrinkId && <DetailsModal id={currentDrinkId} onHide={() => setCurrentDrinkId(null)} />}
    </LoadingPanel>
  );
};
