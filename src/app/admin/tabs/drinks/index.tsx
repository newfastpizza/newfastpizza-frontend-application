import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { DrinksTable } from './table';

export const DrinksTabIndex: React.FC = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={match.url} component={DrinksTable} />
      <Redirect to={match.url} />
    </Switch>
  );
};
