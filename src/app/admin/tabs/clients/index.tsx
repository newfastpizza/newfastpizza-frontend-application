import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { ClientsTable } from './table';

export const ClientsTabIndex: React.FC = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={match.url} component={ClientsTable} />
      <Redirect to={match.url} />
    </Switch>
  );
};
