import React from 'react';
import { useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { useDispatch, useSelector } from 'react-redux';

import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { useCleaning } from '../../../../../core/customHooks/useCleaning';
import { RootState } from '../../../../../core/store';
import { getClients, resetClients } from '../../../../../data/clients/slice';

export const ClientsTable: React.FC = () => {
  useCleaning(resetClients);

  const dispatch = useDispatch();
  const clients = useSelector((store: RootState) => store.clients.items);

  useEffect(() => {
    dispatch(getClients());
  }, [dispatch]);

  return (
    <LoadingPanel asyncThunks={[getClients]}>
      <Table hover>
        <thead>
          <tr>
            <th>Username</th>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
            <th>Birthday</th>
          </tr>
        </thead>
        <tbody>
          {clients?.map((x) => (
            <tr key={x.id}>
              <td>{x.username}</td>
              <td>{x.name}</td>
              <td>{x.email}</td>
              <td>{x.phone}</td>
              <td>{x.birthday ? new Date(x.birthday).toLocaleDateString() : ''}</td>
            </tr>
          ))}
        </tbody>
      </Table>
    </LoadingPanel>
  );
};
