import { unwrapResult } from '@reduxjs/toolkit';
import React, { useCallback, useEffect, useState } from 'react';
import { Button, Col, Form, Modal } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import * as yup from 'yup';

import { TextAreaField } from '../../../../../common/fields/textAreaField';
import { TextBoxField } from '../../../../../common/fields/textBoxField';
import { DeleteModal } from '../../../../../common/parts/deleteModal';
import { LoadingButton } from '../../../../../common/parts/loadingButton';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { showAlert } from '../../../../../data/alerts/slice';
import { Pizza } from '../../../../../data/products/models';
import { deletePizzaById, updatePizza } from '../../../../../data/products/slice';
import { areEqualShallow } from '../../../../../utils/object';

const schema = yup.object().shape({
  name: yup.string().required().label('Name'),
  description: yup.string().required().label('Description'),
  image: yup.string().required().label('Image'),
  size: yup.string().required().label('Size'),
  price: yup
    .number()
    .integer()
    .transform((value) => (isNaN(value) ? undefined : value))
    .required()
    .min(1)
    .label('Price'),
});

interface Props {
  id: string;
  onHide: () => void;
}

export const DetailsModal: React.FC<Props> = ({ id, onHide }) => {
  const dispatch = useAppDispatch();

  const item = useSelector((state: RootState) => state.products.pizzas.find((x) => x.id === id));
  const [details, setDetails] = useState<Pizza>({ id: '', name: '', size: '', price: 1, image: '', description: '' });

  useEffect(() => {
    if (item) {
      setDetails({ ...item });
    }
  }, [item]);

  const [validate, setValidate] = useState(false);
  const isDetailsChanges = !areEqualShallow(item, details);

  const onChange = useCallback((field: keyof Pizza, value: any) => {
    setDetails((form) => ({ ...form, [field]: value }));
  }, []);

  const onUpdateClick = useCallback(() => {
    schema
      .validate(details)
      .then(() =>
        dispatch(updatePizza(details))
          .then(unwrapResult)
          .then(() => {
            dispatch(showAlert({ message: "Product's details has been successfully updated" }));
          })
      )
      .catch(() => setValidate(true));
  }, [details, dispatch]);

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const onDelete = useCallback(() => {
    dispatch(deletePizzaById(details.id))
      .then(unwrapResult)
      .then(() => {
        dispatch(showAlert({ message: 'Product has been deleted', type: 'warning' }));
        onHide();
      });
  }, [dispatch, onHide, details.id]);

  return (
    <>
      <Modal show={!showDeleteModal} onHide={onHide} animation={false} centered>
        <Modal.Header closeButton>
          <Modal.Title>Pizza details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Row>
            <Col>
              <TextBoxField
                controlId="name"
                value={details.name}
                onChange={(v) => onChange('name', v)}
                validate={validate}
                schema={schema}
                fieldPath="name">
                Name
              </TextBoxField>
            </Col>
            <Col>
              <TextBoxField
                controlId="size"
                value={details.size}
                onChange={(v) => onChange('size', v)}
                validate={validate}
                schema={schema}
                fieldPath="size">
                Size
              </TextBoxField>
            </Col>
          </Form.Row>
          <Form.Row>
            <Col>
              <TextBoxField
                type="number"
                controlId="price"
                value={details.price}
                onChange={(v) => onChange('price', v)}
                validate={validate}
                schema={schema}
                fieldPath="price">
                Price
              </TextBoxField>
            </Col>
            <Col>
              <TextBoxField
                controlId="image"
                value={details.image}
                onChange={(v) => onChange('image', v)}
                validate={validate}
                schema={schema}
                fieldPath="image">
                Image
              </TextBoxField>
            </Col>
          </Form.Row>
          <TextAreaField
            controlId="description"
            value={details.description}
            onChange={(v) => onChange('description', v)}
            validate={validate}
            schema={schema}
            fieldPath="description">
            Description
          </TextAreaField>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => setShowDeleteModal(true)} className="mr-auto" variant="danger">
            Delete product
          </Button>
          {isDetailsChanges && (
            <LoadingButton asyncThunks={[updatePizza]} onClick={onUpdateClick}>
              Update details
            </LoadingButton>
          )}
        </Modal.Footer>
      </Modal>
      {showDeleteModal && (
        <DeleteModal
          title="Delete product"
          asyncThunk={deletePizzaById}
          onHide={() => setShowDeleteModal(false)}
          onDelete={onDelete}
        />
      )}
    </>
  );
};
