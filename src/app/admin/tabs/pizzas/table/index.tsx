import React, { useState } from 'react';
import { useEffect } from 'react';
import { Button, Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { useCleaning } from '../../../../../core/customHooks/useCleaning';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { getAllPizzas, resetPizzas } from '../../../../../data/products/slice';
import { DetailsModal } from '../detailsModal';

export const PizzasTable: React.FC = () => {
  useCleaning(resetPizzas);

  const dispatch = useAppDispatch();
  const pizzas = useSelector((store: RootState) => store.products.pizzas);

  useEffect(() => {
    dispatch(getAllPizzas());
  }, [dispatch]);

  const [currentPizzaId, setCurrentPizzaId] = useState<string | null>(null);

  return (
    <LoadingPanel asyncThunks={[getAllPizzas]}>
      <Table hover>
        <thead>
          <tr>
            <th>Name</th>
            <th>Price</th>
            <th>Size</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {pizzas?.map((x) => (
            <tr key={x.id}>
              <td>{x.name}</td>
              <td>{x.price}</td>
              <td>{x.size}</td>
              <td>
                <Button onClick={() => setCurrentPizzaId(x.id)} variant="secondary" size="sm">
                  Details
                </Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
      {currentPizzaId && <DetailsModal id={currentPizzaId} onHide={() => setCurrentPizzaId(null)} />}
    </LoadingPanel>
  );
};
