import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { PizzasTable } from './table';

export const PizzasTabIndex: React.FC = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={match.url} component={PizzasTable} />
      <Redirect to={match.url} />
    </Switch>
  );
};
