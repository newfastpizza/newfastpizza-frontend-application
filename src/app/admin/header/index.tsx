import React, { useCallback } from 'react';
import { Button, Nav } from 'react-bootstrap';
import { useDispatch } from 'react-redux';
import { NavLink, useRouteMatch } from 'react-router-dom';

import { resetToken } from '../../../data/auth/slice';
import { resetOwnProfile } from '../../../data/clients/slice';

export const Header: React.FC = () => {
  const match = useRouteMatch();
  const dispatch = useDispatch();

  const onSignOutClick = useCallback(() => {
    dispatch(resetOwnProfile());
    dispatch(resetToken());
    localStorage.removeItem('token');
  }, [dispatch]);

  return (
    <div>
      <div className="d-flex justify-content-between align-items-center">
        <h2>New Fast Pizza c:</h2>
        <Button onClick={onSignOutClick} size="sm">
          Sign out
        </Button>
      </div>
      <Nav variant="tabs">
        <Nav.Item>
          <NavLink className="nav-link" to={`${match.url}/clients/`}>
            Clients
          </NavLink>
        </Nav.Item>
        <Nav.Item>
          <NavLink className="nav-link" to={`${match.url}/drivers/`}>
            Drivers
          </NavLink>
        </Nav.Item>
        <Nav.Item>
          <NavLink className="nav-link" to={`${match.url}/pizzas/`}>
            Pizzas
          </NavLink>
        </Nav.Item>
        <Nav.Item>
          <NavLink className="nav-link" to={`${match.url}/drinks/`}>
            Drinks
          </NavLink>
        </Nav.Item>
        <Nav.Item>
          <NavLink className="nav-link" to={`${match.url}/townPoints/`}>
            TownPoints
          </NavLink>
        </Nav.Item>
        <Nav.Item>
          <NavLink className="nav-link" to={`${match.url}/orders/`}>
            Orders
          </NavLink>
        </Nav.Item>
      </Nav>
    </div>
  );
};
