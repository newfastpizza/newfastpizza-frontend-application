import React from 'react';
import { Container } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { RootState } from '../../core/store';
import { Header } from './header';
import { SignIn } from './signIn';
import { ClientsTabIndex } from './tabs/clients';
import { DrinksTabIndex } from './tabs/drinks';
import { DriversTabIndex } from './tabs/drivers';
import { OrdersTabIndex } from './tabs/orders';
import { PizzasTabIndex } from './tabs/pizzas';
import { TownPointsTabIndex } from './tabs/townPoints';

export const AdminIndex: React.FC = () => {
  const match = useRouteMatch();
  const isAuth = useSelector((store: RootState) => store.auth.token !== null);

  return (
    <Container>
      <Header />
      <Switch>
        <Route exact path={`${match.url}/signIn`} component={SignIn} />
        {!isAuth && <Redirect to={`${match.url}/signIn/`} />}
        <Route exact path={`${match.url}/clients/`} component={ClientsTabIndex} />
        <Route exact path={`${match.url}/drivers/`} component={DriversTabIndex} />
        <Route exact path={`${match.url}/pizzas/`} component={PizzasTabIndex} />
        <Route exact path={`${match.url}/drinks/`} component={DrinksTabIndex} />
        <Route exact path={`${match.url}/townPoints/`} component={TownPointsTabIndex} />
        <Route exact path={`${match.url}/orders/`} component={OrdersTabIndex} />
        <Redirect to={`${match.url}/clients/`} />
      </Switch>
    </Container>
  );
};
