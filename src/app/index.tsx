import './index.css';

import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

import { AdminIndex } from './admin';
import { ClientIndex } from './client';

export const App: React.FC = () => {
  return (
    <Switch>
      <Route path="/admin" component={AdminIndex} />
      <Route path="/" component={ClientIndex} />
      <Redirect to="/"></Redirect>
    </Switch>
  );
};

export default App;
