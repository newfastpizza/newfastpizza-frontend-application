import './index.css';

import { unwrapResult } from '@reduxjs/toolkit';
import React, { useCallback, useEffect, useState } from 'react';
import { Button, Col, Form, Table } from 'react-bootstrap';
import { ButtonGroup } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';

import { Icon } from '../../../common/base/icon';
import { SelectField } from '../../../common/fields/selectField';
import { TextAreaField } from '../../../common/fields/textAreaField';
import { TextBoxField } from '../../../common/fields/textBoxField';
import { LoadingPanel } from '../../../common/parts/loadingPanel';
import { RootState, useAppDispatch } from '../../../core/store';
import { showAlert } from '../../../data/alerts/slice';
import { getOwnProfile } from '../../../data/clients/slice';
import {
  changeOrderRequest,
  createOrder,
  decProductCountInOrder,
  deleteProductFromOrder,
  incProductCountInOrder,
  resetOrderRequest,
} from '../../../data/orders/slice';
import { Product } from '../../../data/products/models';
import { getAllDrinks, getAllPizzas } from '../../../data/products/slice';
import { getAllTownPoints } from '../../../data/town/slice';
import { phoneRegex } from '../../../utils/regex';

const schema = yup.object().shape({
  townPointId: yup.string().required().label('Address'),
  flatNumber: yup
    .number()
    .integer()
    .transform((value) => (isNaN(value) ? undefined : value))
    .required()
    .min(1)
    .label('Flat number'),
  phone: yup
    .string()
    .required()
    .test({
      test: (v: string) => phoneRegex.test(v),
      message: 'Phone should have phone format',
    } as yup.TestConfig)
    .label('Phone'),
});

export const Cart: React.FC = () => {
  const dispatch = useAppDispatch();
  const history = useHistory();

  const isAuth = useSelector((store: RootState) => store.auth.token !== null);
  const order = useSelector((store: RootState) => store.orders.orderRequest);
  const pizzas = useSelector((store: RootState) => store.products.pizzas);
  const drinks = useSelector((store: RootState) => store.products.drinks);
  const townPoints = useSelector((store: RootState) => store.town.points);

  useEffect(() => {
    if (isAuth) {
      dispatch(getOwnProfile());
    }
    dispatch(getAllPizzas());
    dispatch(getAllDrinks());
    dispatch(getAllTownPoints());
  }, [dispatch, isAuth]);

  const hasProducts = Object.keys(order.productsIds).length > 0;
  let totalCost = 0;

  const [validate, setValidate] = useState(false);

  const onCheckoutClick = useCallback(() => {
    schema
      .validate(order)
      .then(() =>
        dispatch(createOrder(order))
          .then(unwrapResult)
          .then(() => {
            dispatch(showAlert({ message: 'Your order is accepted!' }));
            dispatch(resetOrderRequest());
            history.push(isAuth ? '/orders' : '/');
          })
      )
      .catch(() => setValidate(true));
  }, [dispatch, history, isAuth, order]);

  return (
    <LoadingPanel asyncThunks={[getOwnProfile, getAllPizzas, getAllDrinks, getAllTownPoints]}>
      <div className="cart-container mx-auto py-3">
        {hasProducts && <h4>Products</h4>}
        <Table>
          <tbody>
            {Object.entries(order.productsIds).map((x) => {
              let product: (Product & { info: string }) | undefined;

              const pizza = pizzas.find((y) => y.id === x[0]);
              if (pizza) {
                product = { ...pizza, info: pizza.size };
              } else {
                const drink = drinks.find((y) => y.id === x[0]);
                if (drink) {
                  product = { ...drink, info: `${drink.volume} L` };
                }
              }

              if (!product) {
                return null;
              }

              let cost = x[1] * product.price;
              totalCost += cost;

              return (
                <tr key={x[0]}>
                  <td>
                    <b>{product.name}</b> {product.info}
                  </td>
                  <td>
                    <ButtonGroup size="sm">
                      <Button onClick={() => dispatch(decProductCountInOrder(x[0]))} variant="light">
                        <Icon name="minus" />
                      </Button>
                      <Button variant="light">{x[1]}</Button>
                      <Button onClick={() => dispatch(incProductCountInOrder(x[0]))} variant="light">
                        <Icon name="plus" />
                      </Button>
                    </ButtonGroup>
                  </td>
                  <td>${cost}</td>
                  <td>
                    <button type="button" className="btn" onClick={() => dispatch(deleteProductFromOrder(x[0]))}>
                      <Icon name="trash-alt" />
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </Table>
        <Form.Row>
          <Col>
            <SelectField
              controlId="townPointId"
              options={townPoints}
              value={order.townPointId}
              getLabel={(x) => `${x.street} ${x.building}`}
              getValue={(x) => x.id}
              onChange={(v) => dispatch(changeOrderRequest({ key: 'townPointId', value: v }))}
              addEmptyOption
              validate={validate}
              schema={schema}
              fieldPath="townPointId">
              Address
            </SelectField>
          </Col>
          <Col>
            <TextBoxField
              controlId="flatNumber"
              type="number"
              value={order.flatNumber < 1 ? undefined : order.flatNumber}
              onChange={(v) => dispatch(changeOrderRequest({ key: 'flatNumber', value: Number(v) }))}
              validate={validate}
              schema={schema}
              fieldPath="flatNumber">
              Flat number
            </TextBoxField>
          </Col>
        </Form.Row>
        <Form.Row>
          <Col>
            <TextBoxField
              controlId="phone"
              value={order.phone}
              onChange={(v) => dispatch(changeOrderRequest({ key: 'phone', value: v }))}
              validate={validate}
              schema={schema}
              fieldPath="phone">
              Phone
            </TextBoxField>
          </Col>
          <Col>
            <TextAreaField
              controlId="comment"
              value={order.comment}
              onChange={(v) => dispatch(changeOrderRequest({ key: 'comment', value: v }))}>
              Comment
            </TextAreaField>
          </Col>
        </Form.Row>
        <div className="d-flex justify-content-between align-items-center my-4">
          <h5>
            <b>Total cost:</b> ${totalCost}
          </h5>
          <Button onClick={onCheckoutClick} variant="success" disabled={!hasProducts}>
            Checkout
          </Button>
        </div>
      </div>
    </LoadingPanel>
  );
};
