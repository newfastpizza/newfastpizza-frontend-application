import React from 'react';
import { Container } from 'react-bootstrap';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { Cart } from './cart';
import { Header } from './header';
import { DrinksList } from './tabs/drinks/list';
import { OrdersTabIndex } from './tabs/orders';
import { PizzasList } from './tabs/pizzas/list';

export const ClientIndex: React.FC = () => {
  const match = useRouteMatch();
  return (
    <Container fluid="xl">
      <Header />
      <Switch>
        <Route exact path={`${match.url}pizzas/`} component={PizzasList} />
        <Route exact path={`${match.url}drinks/`} component={DrinksList} />
        <Route exact path={`${match.url}cart`} component={Cart} />
        <Route exact path={`${match.url}orders/`} component={OrdersTabIndex} />
        <Redirect to={`${match.url}pizzas/`} />
      </Switch>
    </Container>
  );
};
