import { unwrapResult } from '@reduxjs/toolkit';
import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { Button, Col, Form, Modal } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { useHistory } from 'react-router-dom';
import * as yup from 'yup';

import { TextBoxField } from '../../../common/fields/textBoxField';
import { DeleteModal } from '../../../common/parts/deleteModal';
import { LoadingButton } from '../../../common/parts/loadingButton';
import { LoadingPanel } from '../../../common/parts/loadingPanel';
import { useCleaning } from '../../../core/customHooks/useCleaning';
import { RootState, useAppDispatch } from '../../../core/store';
import { showAlert } from '../../../data/alerts/slice';
import { changePassword, resetToken } from '../../../data/auth/slice';
import { ChangeClientProfileRequest, ChangePasswordRequest } from '../../../data/clients/models';
import { deleteClient, getOwnProfile, resetOwnProfile, updateClientInfo } from '../../../data/clients/slice';
import { nameRegex, passwordRegex, phoneRegex } from '../../../utils/regex';

const clientInfoSchema = yup.object().shape({
  name: yup
    .string()
    .required()
    .test({
      test: (v: string) => nameRegex.test(v),
      message: 'Name can consist of uppercase and lowercase latin letters or spaces',
    } as yup.TestConfig)
    .label('Name'),
  email: yup.string().email().label('Email'),
  phone: yup
    .string()
    .required()
    .test({
      test: (v: string) => phoneRegex.test(v),
      message: 'Phone should have phone format',
    } as yup.TestConfig)
    .label('Phone'),
});

interface Props {
  onHide: () => void;
}

export const ProfileModal: React.FC<Props> = ({ onHide }) => {
  useCleaning(resetOwnProfile);

  const history = useHistory();
  const dispatch = useAppDispatch();

  const profile = useSelector((store: RootState) => store.clients.ownProfile);

  const [clientInfo, setClientInfo] = useState<ChangeClientProfileRequest>({
    name: '',
    email: '',
    phone: '',
  });

  const isClientInfoChanges =
    profile &&
    (profile.name !== clientInfo.name || profile.email !== clientInfo.email || profile.phone !== clientInfo.phone);

  useEffect(() => {
    dispatch(getOwnProfile())
      .then(unwrapResult)
      .then((x) => setClientInfo({ name: x.name, email: x.email, phone: x.phone }));
  }, [dispatch]);

  const [validateClientInfo, setValidateClientInfo] = useState(false);

  const onChangeClientInfo = useCallback((field: keyof ChangeClientProfileRequest, value: string) => {
    setClientInfo((form) => ({ ...form, [field]: value }));
  }, []);

  const onUpdateProfileClick = useCallback(() => {
    clientInfoSchema
      .validate(clientInfo)
      .then(() =>
        dispatch(updateClientInfo(clientInfo))
          .then(unwrapResult)
          .then(() => dispatch(showAlert({ message: 'Your profile has been successfully updated' })))
      )
      .catch(() => setValidateClientInfo(true));
  }, [clientInfo, dispatch]);

  const [passwordFields, setPasswordFields] = useState<ChangePasswordRequest>({
    oldPassword: '',
    password: '',
    confirmPassword: '',
  });

  const [validatePassword, setValidatePassword] = useState(false);

  const passwordSchema = useMemo(
    () =>
      yup.object().shape({
        password: yup
          .string()
          .required()
          .min(5)
          .test({
            test: (v: string) => passwordRegex.test(v),
            message: 'Password can consist of uppercase and lowercase latin letters or spec symbols',
          } as yup.TestConfig)
          .label('Password'),
        confirmPassword: yup
          .string()
          .oneOf([passwordFields.password, null], 'The field does not match the password')
          .label('Password'),
      }),
    [passwordFields.password]
  );

  const onChangePasswordFields = useCallback((field: keyof ChangePasswordRequest, value: string) => {
    setPasswordFields((form) => ({ ...form, [field]: value }));
  }, []);

  const [isChangingPassword, setIsChangingPassword] = useState(false);

  const onChangePasswordClick = useCallback(() => {
    passwordSchema
      .validate(passwordFields)
      .then(() =>
        dispatch(changePassword(passwordFields))
          .then(unwrapResult)
          .then(() => {
            dispatch(showAlert({ message: 'Your password has been successfully changed' }));
            setPasswordFields({ oldPassword: '', password: '', confirmPassword: '' });
            setIsChangingPassword(false);
            setValidatePassword(false);
          })
      )
      .catch(() => setValidatePassword(true));
  }, [passwordFields, dispatch, passwordSchema]);

  const onSignOutClick = useCallback(() => {
    dispatch(resetToken());
    localStorage.removeItem('token');
    history.push('/');
    onHide();
  }, [dispatch, history, onHide]);

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  const onDelete = useCallback(() => {
    dispatch(deleteClient())
      .then(unwrapResult)
      .then(() => {
        dispatch(showAlert({ message: 'Your profile has been deleted', type: 'warning' }));
        onSignOutClick();
      });
  }, [dispatch, onSignOutClick]);

  return (
    <>
      <Modal show={!showDeleteModal} onHide={onHide} animation={false} centered>
        <Modal.Header closeButton>
          <Modal.Title>Profile</Modal.Title>
        </Modal.Header>
        <LoadingPanel asyncThunks={[getOwnProfile]}>
          <Modal.Body>
            <Form.Row>
              <Col>
                <TextBoxField controlId="username" value={profile?.username} disabled>
                  Username
                </TextBoxField>
              </Col>
              <Col>
                <TextBoxField
                  controlId="name"
                  value={clientInfo.name}
                  onChange={(v) => onChangeClientInfo('name', v)}
                  validate={validateClientInfo}
                  schema={clientInfoSchema}
                  fieldPath="name">
                  Name
                </TextBoxField>
              </Col>
            </Form.Row>
            <Form.Row>
              <Col>
                <TextBoxField
                  controlId="email"
                  value={clientInfo.email}
                  onChange={(v) => onChangeClientInfo('email', v)}
                  validate={validateClientInfo}
                  schema={clientInfoSchema}
                  fieldPath="email">
                  Email
                </TextBoxField>
              </Col>
              <Col>
                <TextBoxField
                  controlId="phone"
                  value={clientInfo.phone}
                  onChange={(v) => onChangeClientInfo('phone', v)}
                  validate={validateClientInfo}
                  schema={clientInfoSchema}
                  fieldPath="phone">
                  Phone
                </TextBoxField>
              </Col>
            </Form.Row>
            <TextBoxField
              controlId="birthday"
              value={profile?.birthday ? new Date(profile.birthday).toLocaleDateString() : ''}
              disabled>
              Birthday
            </TextBoxField>
            {!isChangingPassword && <Button onClick={() => setIsChangingPassword(true)}>New password</Button>}
            {isChangingPassword && (
              <>
                <Form.Row>
                  <Col xs={6}>
                    <TextBoxField
                      controlId="oldPassword"
                      value={passwordFields.oldPassword}
                      onChange={(v) => onChangePasswordFields('oldPassword', v)}
                      type="password">
                      Old password
                    </TextBoxField>
                  </Col>
                </Form.Row>
                <Form.Row>
                  <Col>
                    <TextBoxField
                      controlId="password"
                      value={passwordFields.password}
                      onChange={(v) => onChangePasswordFields('password', v)}
                      type="password"
                      validate={validatePassword}
                      schema={passwordSchema}
                      fieldPath="password">
                      New password
                    </TextBoxField>
                  </Col>
                  <Col>
                    <TextBoxField
                      controlId="confirmPassword"
                      value={passwordFields.confirmPassword}
                      onChange={(v) => onChangePasswordFields('confirmPassword', v)}
                      type="password"
                      validate={validatePassword}
                      schema={passwordSchema}
                      fieldPath="confirmPassword">
                      Confirm password
                    </TextBoxField>
                  </Col>
                </Form.Row>
                <LoadingButton asyncThunks={[changePassword]} onClick={onChangePasswordClick}>
                  Change password
                </LoadingButton>
              </>
            )}
          </Modal.Body>
        </LoadingPanel>
        <Modal.Footer>
          <div className="d-flex mr-auto">
            <Button onClick={() => setShowDeleteModal(true)} className="mr-3" variant="danger">
              Delete profile
            </Button>
            {isClientInfoChanges && (
              <LoadingButton asyncThunks={[updateClientInfo]} onClick={onUpdateProfileClick}>
                Update profile
              </LoadingButton>
            )}
          </div>
          <Button onClick={onSignOutClick} variant="link">
            Sign out
          </Button>
        </Modal.Footer>
      </Modal>
      {showDeleteModal && (
        <DeleteModal
          title="Delete profile"
          asyncThunk={deleteClient}
          onHide={() => setShowDeleteModal(false)}
          onDelete={onDelete}
        />
      )}
    </>
  );
};
