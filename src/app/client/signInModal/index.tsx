import { unwrapResult } from '@reduxjs/toolkit';
import React, { useCallback, useState } from 'react';
import { Button, Modal } from 'react-bootstrap';
import * as yup from 'yup';

import { TextBoxField } from '../../../common/fields/textBoxField';
import { LoadingButton } from '../../../common/parts/loadingButton';
import { useAppDispatch } from '../../../core/store';
import { SignInRequest } from '../../../data/auth/models';
import { signIn } from '../../../data/auth/slice';
import { SignUpClientModal } from '../signUpClientModal';

const schema = yup.object().shape({
  username: yup.string().required().label('Username'),
  password: yup.string().required().label('Password'),
});

interface Props {
  onHide: () => void;
}

export const SignInModal: React.FC<Props> = ({ onHide }) => {
  const dispatch = useAppDispatch();

  const [form, setForm] = useState<SignInRequest>({
    username: '',
    password: '',
  });

  const onChange = useCallback((field: keyof SignInRequest, value: string) => {
    setForm((form) => ({ ...form, [field]: value }));
  }, []);

  const [showSignUpModal, setShowSignUpModal] = useState(false);
  const [validate, setValidate] = useState(false);

  const onSignInClick = useCallback(() => {
    schema
      .validate(form)
      .then(() => dispatch(signIn(form)).then(unwrapResult).then(onHide))
      .catch(() => setValidate(true));
  }, [dispatch, form, onHide]);

  return (
    <Modal show onHide={onHide} animation={false} centered size="sm">
      <Modal.Header closeButton>
        <Modal.Title>Sign in</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <TextBoxField
          controlId="username"
          value={form.username}
          onChange={(v) => onChange('username', v)}
          validate={validate}
          schema={schema}
          fieldPath="username">
          Username
        </TextBoxField>
        <TextBoxField
          controlId="password"
          value={form.password}
          onChange={(v) => onChange('password', v)}
          type="password"
          validate={validate}
          schema={schema}
          fieldPath="password">
          Password
        </TextBoxField>
      </Modal.Body>
      <Modal.Footer>
        <LoadingButton asyncThunks={[signIn]} onClick={onSignInClick} className="mr-auto">
          Sign in
        </LoadingButton>
        <Button onClick={() => setShowSignUpModal(true)} variant="link">
          Sign up
        </Button>
      </Modal.Footer>
      {showSignUpModal && <SignUpClientModal onHide={() => setShowSignUpModal(false)} />}
    </Modal>
  );
};
