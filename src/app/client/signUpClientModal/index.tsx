import { unwrapResult } from '@reduxjs/toolkit';
import React, { useCallback, useMemo, useState } from 'react';
import { Col, Form, Modal } from 'react-bootstrap';
import * as yup from 'yup';
import { TestConfig } from 'yup';

import { DateField } from '../../../common/fields/dateField';
import { TextBoxField } from '../../../common/fields/textBoxField';
import { LoadingButton } from '../../../common/parts/loadingButton';
import { useAppDispatch } from '../../../core/store';
import { showAlert } from '../../../data/alerts/slice';
import { SignUpClientRequest } from '../../../data/auth/models';
import { signIn, signUpClient } from '../../../data/auth/slice';
import { nameRegex, passwordRegex, phoneRegex, usernameRegex } from '../../../utils/regex';

const schema = yup.object().shape({
  username: yup
    .string()
    .required()
    .min(3)
    .test({
      test: (v: string) => usernameRegex.test(v),
      message: 'Username can consist of uppercase and lowercase latin letters, numbers or underscores',
    } as TestConfig)
    .label('Username'),
  name: yup
    .string()
    .required()
    .test({
      test: (v: string) => nameRegex.test(v),
      message: 'Name can consist of uppercase and lowercase latin letters or spaces',
    } as TestConfig)
    .label('Name'),
  email: yup.string().email().label('Email'),
  phone: yup
    .string()
    .required()
    .test({
      test: (v: string) => phoneRegex.test(v),
      message: 'Phone should have phone format',
    } as TestConfig)
    .label('Phone'),
  password: yup
    .string()
    .required()
    .min(5)
    .test({
      test: (v: string) => passwordRegex.test(v),
      message: 'Password can consist of uppercase and lowercase latin letters or spec symbols',
    } as TestConfig)
    .label('Password'),
});

interface Props {
  onHide: () => void;
}

export const SignUpClientModal: React.FC<Props> = ({ onHide }) => {
  const dispatch = useAppDispatch();

  const [form, setForm] = useState<SignUpClientRequest>({
    username: '',
    name: '',
    email: '',
    phone: '',
    birthday: '',
    password: '',
    confirmPassword: '',
  });

  const onChange = useCallback((field: keyof SignUpClientRequest, value: any) => {
    setForm((form) => ({ ...form, [field]: value }));
  }, []);

  const [validate, setValidate] = useState(false);

  const confirmPasswordSchema = useMemo(
    () =>
      yup.object().shape({
        confirmPassword: yup
          .string()
          .oneOf([form.password, null], 'The field does not match the password')
          .label('Password'),
      }),
    [form.password]
  );

  const fullSchema = useMemo(() => schema.concat(confirmPasswordSchema), [confirmPasswordSchema]);

  const onSignUpClick = useCallback(() => {
    fullSchema
      .validate(form)
      .then(() =>
        dispatch(signUpClient(form))
          .then(unwrapResult)
          .then(() => {
            dispatch(showAlert({ message: 'You have successfully registered!' }));
            const { username, password } = form;
            dispatch(signIn({ username, password }));
          })
      )
      .catch(() => setValidate(true));
  }, [dispatch, form, fullSchema]);

  return (
    <Modal show onHide={onHide} animation={false} centered>
      <Modal.Header closeButton>
        <Modal.Title>Sign up</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        <Form.Row>
          <Col>
            <TextBoxField
              controlId="username"
              value={form.username}
              onChange={(v) => onChange('username', v)}
              validate={validate}
              schema={fullSchema}
              fieldPath="username">
              Username
            </TextBoxField>
          </Col>
          <Col>
            <TextBoxField
              controlId="name"
              value={form.name}
              onChange={(v) => onChange('name', v)}
              validate={validate}
              schema={fullSchema}
              fieldPath="name">
              Name
            </TextBoxField>
          </Col>
        </Form.Row>
        <Form.Row>
          <Col>
            <TextBoxField
              controlId="email"
              value={form.email}
              onChange={(v) => onChange('email', v)}
              validate={validate}
              schema={fullSchema}
              fieldPath="email">
              Email
            </TextBoxField>
          </Col>
          <Col>
            <TextBoxField
              controlId="phone"
              value={form.phone}
              onChange={(v) => onChange('phone', v)}
              validate={validate}
              schema={fullSchema}
              fieldPath="phone">
              Phone
            </TextBoxField>
          </Col>
        </Form.Row>
        <DateField controlId="birthday" value={form.birthday} onChange={(v) => onChange('birthday', v)}>
          Birthday
        </DateField>
        <Form.Row>
          <Col>
            <TextBoxField
              controlId="password"
              value={form.password}
              onChange={(v) => onChange('password', v)}
              type="password"
              validate={validate}
              schema={fullSchema}
              fieldPath="password">
              Password
            </TextBoxField>
          </Col>
          <Col>
            <TextBoxField
              controlId="confirmPassword"
              value={form.confirmPassword}
              onChange={(v) => onChange('confirmPassword', v)}
              type="password"
              validate={validate}
              schema={fullSchema}
              fieldPath="confirmPassword">
              Confirm password
            </TextBoxField>
          </Col>
        </Form.Row>
      </Modal.Body>
      <Modal.Footer>
        <LoadingButton asyncThunks={[signUpClient, signIn]} onClick={onSignUpClick} className="mr-auto">
          Sign up
        </LoadingButton>
      </Modal.Footer>
    </Modal>
  );
};
