import React, { useEffect, useState } from 'react';
import { Badge } from 'react-bootstrap';
import { Button, Nav, Navbar } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { NavLink, useRouteMatch } from 'react-router-dom';

import { RootState } from '../../../core/store';
import { ProfileModal } from '../profileModal';
import { SignInModal } from '../signInModal';

export const Header: React.FC = () => {
  const match = useRouteMatch();
  const isAuth = useSelector((store: RootState) => store.auth.token !== null);

  const [showSignInModal, setShowSignInModal] = useState(false);
  const [showProfileModal, setShowProfileModal] = useState(false);

  useEffect(() => {
    if (isAuth) {
      setShowSignInModal(false);
    }
  }, [isAuth]);

  const productIds = useSelector((store: RootState) => store.orders.orderRequest.productsIds);
  const productCount = Object.values(productIds).reduce((p, c) => p + c, 0);

  return (
    <div className="client-header">
      <div className="d-flex justify-content-between align-items-center">
        <h1>New Fast Pizza!</h1>
        {isAuth ? (
          <Button onClick={() => setShowProfileModal(true)} size="sm" variant="light">
            Profile
          </Button>
        ) : (
          <Button onClick={() => setShowSignInModal(true)} size="sm" variant="light">
            Sign in
          </Button>
        )}
      </div>
      <Navbar bg="light" variant="light">
        <Nav className="flex-nowrap">
          <Nav.Item>
            <NavLink className="nav-link" to={`${match.url}pizzas/`}>
              Pizzas
            </NavLink>
          </Nav.Item>
          <Nav.Item>
            <NavLink className="nav-link" to={`${match.url}drinks/`}>
              Drinks
            </NavLink>
          </Nav.Item>
          {isAuth && (
            <Nav.Item>
              <NavLink className="nav-link" to={`${match.url}orders/`}>
                Orders
              </NavLink>
            </Nav.Item>
          )}
        </Nav>
        <NavLink to={`${match.url}cart`} className="btn btn-primary ml-auto">
          Cart {productCount > 0 && <Badge variant="light">{productCount}</Badge>}
        </NavLink>
      </Navbar>
      {showSignInModal && <SignInModal onHide={() => setShowSignInModal(false)} />}
      {showProfileModal && <ProfileModal onHide={() => setShowProfileModal(false)} />}
    </div>
  );
};
