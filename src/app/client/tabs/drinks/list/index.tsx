import React from 'react';
import { useEffect } from 'react';
import { Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { getAllDrinks } from '../../../../../data/products/slice';
import { ListItem } from '../listItem';

export const DrinksList: React.FC = () => {
  const dispatch = useAppDispatch();
  const drinks = useSelector((store: RootState) => store.products.drinks);

  useEffect(() => {
    dispatch(getAllDrinks());
  }, [dispatch]);

  return (
    <LoadingPanel asyncThunks={[getAllDrinks]}>
      <Row className="d-flex">
        {drinks.map((x) => (
          <ListItem key={x.id} item={x} />
        ))}
      </Row>
    </LoadingPanel>
  );
};
