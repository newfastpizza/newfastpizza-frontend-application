import './index.css';

import React, { useCallback } from 'react';
import { Button, Col } from 'react-bootstrap';
import { useSelector } from 'react-redux';
import { NavLink } from 'react-router-dom';

import { RootState, useAppDispatch } from '../../../../../core/store';
import { addProductToOrder } from '../../../../../data/orders/slice';
import { Drink } from '../../../../../data/products/models';

export const ListItem: React.FC<{ item: Drink }> = ({ item }) => {
  const dispatch = useAppDispatch();

  const addToCart = useCallback(() => {
    dispatch(addProductToOrder(item.id));
  }, [dispatch, item.id]);

  const productInCart = useSelector((store: RootState) => store.orders.orderRequest.productsIds[item.id] !== undefined);

  return (
    <Col xs={6} md={4} lg={3} xl={2} className="drink-item d-flex flex-column py-3">
      <img src={item.image} alt={item.name + 'image'} />
      <div className="d-flex justify-content-between">
        <h5>{item.name}</h5>
        <div className="volume">{item.volume + ' L'}</div>
      </div>
      <div>{item.description}</div>
      <div className="d-flex justify-content-between align-items-end mt-2">
        <div>${item.price}</div>
        {!productInCart && (
          <Button onClick={addToCart} size="sm">
            Add to cart
          </Button>
        )}
        {productInCart && (
          <NavLink to="/cart" className="btn btn-success btn-sm">
            Go to cart
          </NavLink>
        )}
      </div>
    </Col>
  );
};
