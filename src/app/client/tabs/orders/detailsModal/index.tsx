import React, { useEffect } from 'react';
import { Table } from 'react-bootstrap';
import { Col, Form, Modal } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { TextAreaField } from '../../../../../common/fields/textAreaField';
import { TextBoxField } from '../../../../../common/fields/textBoxField';
import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { getOrderById } from '../../../../../data/orders/slice';
import { TownPoint } from '../../../../../data/town/models';

const getAddress = (townPoint?: TownPoint | null) => {
  return townPoint ? `${townPoint.street} ${townPoint.building}` : null;
};

interface Props {
  id: string;
  onHide: () => void;
}

export const DetailsModal: React.FC<Props> = ({ id, onHide }) => {
  const dispatch = useAppDispatch();

  const order = useSelector((state: RootState) => state.orders.item.order);

  useEffect(() => {
    dispatch(getOrderById(id));
  }, [dispatch, id]);

  const date = order?.date ? new Date(order?.date) : null;

  return (
    <LoadingPanel asyncThunks={[getOrderById]}>
      <Modal show onHide={onHide} animation={false} centered>
        <Modal.Header closeButton>
          <Modal.Title>Order details</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form.Row>
            <Col>
              <TextBoxField controlId="address" value={getAddress(order?.townPoint)} disabled>
                Address
              </TextBoxField>
            </Col>
            <Col>
              <TextBoxField controlId="flatNumber" value={order?.flatNumber} disabled>
                Flat number
              </TextBoxField>
            </Col>
          </Form.Row>
          <Form.Row>
            <Col>
              <TextBoxField
                controlId="date"
                value={date ? `${date.toLocaleDateString()} ${date.toLocaleTimeString()}` : null}
                disabled>
                Date
              </TextBoxField>
            </Col>
            <Col>
              <TextAreaField controlId="comment" value={order?.comment} disabled>
                Comment
              </TextAreaField>
            </Col>
          </Form.Row>
          <Form.Row>
            <Col>
              <TextBoxField controlId="status" value={order?.status} disabled>
                Status
              </TextBoxField>
            </Col>
            <Col>
              <TextBoxField controlId="phone" value={order?.phone} disabled>
                Phone
              </TextBoxField>
            </Col>
          </Form.Row>
          <Table hover>
            <thead>
              <tr>
                <th>Product</th>
                <th>Count</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {order?.backet?.map((x) => (
                <tr key={x.id}>
                  <td>{x.product.name}</td>
                  <td>{x.count}</td>
                  <td>${x.price}</td>
                </tr>
              ))}
              <tr className="table-success">
                <td>Total</td>
                <td>{order?.backet?.reduce((p, c) => p + c.count, 0)}</td>
                <td>${order?.backet?.reduce((p, c) => p + c.price, 0)}</td>
              </tr>
            </tbody>
          </Table>
        </Modal.Body>
      </Modal>
    </LoadingPanel>
  );
};
