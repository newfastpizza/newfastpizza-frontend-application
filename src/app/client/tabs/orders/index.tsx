import React from 'react';
import { Redirect, Route, Switch, useRouteMatch } from 'react-router-dom';

import { OrdersTable } from './table';

export const OrdersTabIndex: React.FC = () => {
  const match = useRouteMatch();
  return (
    <Switch>
      <Route exact path={match.url} component={OrdersTable} />
      <Redirect to={match.url} />
    </Switch>
  );
};
