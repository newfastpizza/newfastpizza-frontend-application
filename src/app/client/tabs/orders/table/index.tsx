import React, { useState } from 'react';
import { useEffect } from 'react';
import { Button, Table } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { useCleaning } from '../../../../../core/customHooks/useCleaning';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { getOrders, resetOrders } from '../../../../../data/orders/slice';
import { DetailsModal } from '../detailsModal';

export const OrdersTable: React.FC = () => {
  useCleaning(resetOrders);

  const dispatch = useAppDispatch();
  const items = useSelector((store: RootState) => store.orders.items);

  useEffect(() => {
    dispatch(getOrders());
  }, [dispatch]);

  const [currentOrderId, setCurrentOrderId] = useState<string | null>(null);

  return (
    <LoadingPanel asyncThunks={[getOrders]}>
      <Table hover>
        <thead>
          <tr>
            <th>Phone</th>
            <th>Date</th>
            <th>Status</th>
            <th></th>
          </tr>
        </thead>
        <tbody>
          {items?.map((x) => {
            const date = new Date(x.date);
            return (
              <tr key={x.id}>
                <td>{x.phone}</td>
                <td>{`${date.toLocaleDateString()} ${date.toLocaleTimeString()}`}</td>
                <td>{x.status}</td>
                <td>
                  <Button onClick={() => setCurrentOrderId(x.id)} variant="secondary" size="sm">
                    Details
                  </Button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </Table>
      {currentOrderId && <DetailsModal id={currentOrderId} onHide={() => setCurrentOrderId(null)} />}
    </LoadingPanel>
  );
};
