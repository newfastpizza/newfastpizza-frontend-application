import React from 'react';
import { useEffect } from 'react';
import { Row } from 'react-bootstrap';
import { useSelector } from 'react-redux';

import { LoadingPanel } from '../../../../../common/parts/loadingPanel';
import { RootState, useAppDispatch } from '../../../../../core/store';
import { getAllPizzas } from '../../../../../data/products/slice';
import { ListItem } from '../listItem';

export const PizzasList: React.FC = () => {
  const dispatch = useAppDispatch();
  const pizzas = useSelector((store: RootState) => store.products.pizzas);

  useEffect(() => {
    dispatch(getAllPizzas());
  }, [dispatch]);

  return (
    <LoadingPanel asyncThunks={[getAllPizzas]}>
      <Row className="d-flex">
        {pizzas.map((x) => (
          <ListItem key={x.id} item={x} />
        ))}
      </Row>
    </LoadingPanel>
  );
};
