import { createAsyncThunk, createSlice, isAnyOf, PayloadAction } from '@reduxjs/toolkit';

import { http } from '../../core/http';
import { Client } from '../clients/models';
import { getOwnProfile } from '../clients/slice';
import { Driver } from '../drivers/models';
import { CreateOrderRequest, Order } from './models';

const baseUrl = 'orders';

interface OrdersState {
  items: Order[];
  item: {
    order?: Order;
    client?: Client;
    driver?: Driver;
  };
  orderRequest: CreateOrderRequest;
}

const initialState: OrdersState = {
  items: [],
  item: {},
  orderRequest: {
    productsIds: {},
    townPointId: '',
    flatNumber: -1,
    phone: '',
    comment: '',
  },
};

export const createOrder = createAsyncThunk(`${baseUrl}/createOrder`, async (model: CreateOrderRequest) => {
  await http.request(baseUrl, 'POST', model);
});

export const getOrders = createAsyncThunk(`${baseUrl}/getOrders`, async () => {
  const orders = await http.get<Order[]>(baseUrl);
  return orders.sort((a, b) => b.date.localeCompare(a.date));
});

export const getOrderById = createAsyncThunk(`${baseUrl}/getOrderById`, async (id: string) => {
  return await http.get<Order>(`${baseUrl}/${id}`);
});

export const getClientForOrderById = createAsyncThunk(`${baseUrl}/getClientForOrderById`, async (id: string) => {
  const response = await http.request(`${baseUrl}/${id}/client`, 'GET');
  try {
    return await http.processJsonResponse<Client>(response);
  } catch {
    return undefined;
  }
});

export const getDriverForOrderById = createAsyncThunk(`${baseUrl}/getDriverForOrderById`, async (id: string) => {
  const response = await http.request(`${baseUrl}/${id}/driver`, 'GET');
  try {
    return await http.processJsonResponse<Driver>(response);
  } catch {
    return undefined;
  }
});

export const deleteOrderById = createAsyncThunk(`${baseUrl}/deleteOrderById`, async (id: string) => {
  return await http.delete<Order>(`${baseUrl}/${id}`);
});

export const deliverOrder = createAsyncThunk(`${baseUrl}/deliverOrder`, async (id: string) => {
  return await http.get<Order>(`${baseUrl}/${id}/deliver`);
});

export const finishOrder = createAsyncThunk(`${baseUrl}/finishOrder`, async (id: string) => {
  return await http.get<Order>(`${baseUrl}/${id}/finish`);
});

export const slice = createSlice({
  name: baseUrl,
  initialState,
  reducers: {
    resetOrders: (state) => {
      state.items = initialState.items;
    },
    resetOrder: (state) => {
      state.item = initialState.item;
    },
    resetOrderRequest: (state) => {
      state.orderRequest = initialState.orderRequest;
    },
    changeOrderRequest: (state, action: PayloadAction<{ key: keyof CreateOrderRequest; value: any }>) => {
      const { key, value } = action.payload;
      (state.orderRequest[key] as any) = value;
    },
    addProductToOrder: (state, action: PayloadAction<string>) => {
      state.orderRequest.productsIds[action.payload] = 1;
    },
    deleteProductFromOrder: (state, action: PayloadAction<string>) => {
      delete state.orderRequest.productsIds[action.payload];
    },
    incProductCountInOrder: (state, action: PayloadAction<string>) => {
      const productIds = state.orderRequest.productsIds;
      if (productIds[action.payload] !== undefined) {
        productIds[action.payload]++;
      }
    },
    decProductCountInOrder: (state, action: PayloadAction<string>) => {
      const productIds = state.orderRequest.productsIds;
      if (productIds[action.payload] !== undefined) {
        if (productIds[action.payload] === 1) {
          delete productIds[action.payload];
        } else {
          productIds[action.payload]--;
        }
      }
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getOwnProfile.fulfilled, (state, action) => {
        state.orderRequest.phone = action.payload.phone;
      })
      .addCase(getOrders.fulfilled, (state, action) => {
        state.items = action.payload;
      })
      .addCase(getOrderById.fulfilled, (state, action) => {
        const order = action.payload;
        const index = state.items?.findIndex((x) => x.id === order.id) ?? -1;

        if (state.items && index > -1) {
          state.items[index] = order;
        }

        state.item.order = order;
      })
      .addCase(getClientForOrderById.fulfilled, (state, action) => {
        state.item.client = action.payload;
      })
      .addCase(getDriverForOrderById.fulfilled, (state, action) => {
        state.item.driver = action.payload;
      })
      .addCase(deleteOrderById.fulfilled, (state, action) => {
        const order = action.payload;
        const index = state.items?.findIndex((x) => x.id === order.id) ?? -1;
        if (index > -1) {
          state.items.splice(index, 1);
        }
      })
      .addMatcher(isAnyOf(deliverOrder.fulfilled, finishOrder.fulfilled), (state, action) => {
        const order = action.payload;
        const index = state.items?.findIndex((x) => x.id === order.id) ?? -1;
        if (index > -1) {
          state.items[index] = order;
        }
        state.item.order = order;
      });
  },
});

export const {
  resetOrders,
  resetOrder,
  resetOrderRequest,
  changeOrderRequest,
  addProductToOrder,
  deleteProductFromOrder,
  incProductCountInOrder,
  decProductCountInOrder,
} = slice.actions;
