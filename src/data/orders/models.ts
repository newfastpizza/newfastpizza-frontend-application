import { Product } from '../products/models';
import { TownPoint } from '../town/models';

export interface CreateOrderRequest {
  productsIds: Record<string, number>;
  townPointId: string;
  flatNumber: number;
  phone: string;
  comment: string;
}

export interface Order {
  id: string;
  backet: BacketItem[];
  phone: string;
  townPoint: TownPoint;
  flatNumber: number;
  comment: string;
  date: string;
  status: OrderStatus;
}

export interface BacketItem {
  id: string;
  product: Product;
  count: number;
  price: number;
  pricePerItem: number;
}

export type OrderStatus = 'IS_PREPARED' | 'IS_DELIVERED' | 'IS_FINISHED';
