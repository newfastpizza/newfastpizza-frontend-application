export interface SignInRequest {
  password: string;
  username: string;
}

export interface SignUpClientRequest {
  username: string;
  name: string;
  email: string;
  phone: string;
  birthday?: string;
  password: string;
  confirmPassword: string;
}

export interface SignUpDriverRequest {
  username: string;
  name: string;
  phone: string;
  password: string;
  confirmPassword: string;
}
