import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { http } from '../../core/http';
import { ChangePasswordRequest } from '../clients/models';
import { SignInRequest, SignUpClientRequest, SignUpDriverRequest } from './models';

const baseUrl = 'auth';

interface AuthState {
  token: string | null;
}

const initialState: AuthState = {
  token: localStorage.getItem('token'),
};

export const signIn = createAsyncThunk(`${baseUrl}/signIn`, async (model: SignInRequest) => {
  const response = await http.request(`${baseUrl}/signin`, 'POST', model);
  const token = await http.processTextResponse(response);
  localStorage.setItem('token', token);
  return token;
});

export const signUpClient = createAsyncThunk(`${baseUrl}/signUpClient`, async (model: SignUpClientRequest) => {
  await http.request(`${baseUrl}/signup/client`, 'POST', model);
});

export const signUpDriver = createAsyncThunk(`${baseUrl}/signUpDriver`, async (model: SignUpDriverRequest) => {
  await http.request(`${baseUrl}/signup/driver`, 'POST', model);
});

export const changePassword = createAsyncThunk(`${baseUrl}/changePassword`, async (model: ChangePasswordRequest) => {
  await http.request(`${baseUrl}/password`, 'POST', model);
});

export const slice = createSlice({
  name: baseUrl,
  initialState,
  reducers: {
    resetToken: (state) => {
      state.token = null;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(signIn.fulfilled, (state, action) => {
      state.token = action.payload;
    });
  },
});

export const { resetToken } = slice.actions;
