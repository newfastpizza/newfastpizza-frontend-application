import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { http } from '../../core/http';
import { Drink, Pizza, Product } from './models';

const baseUrl = 'products';

interface ProductsState {
  pizzas: Pizza[];
  drinks: Drink[];
}

const initialState: ProductsState = {
  pizzas: [],
  drinks: [],
};

export const getAllPizzas = createAsyncThunk(`${baseUrl}/getAllPizzas`, async () => {
  return await http.get<Pizza[]>(`${baseUrl}/pizzas`);
});

export const getAllDrinks = createAsyncThunk(`${baseUrl}/getAllDrinks`, async () => {
  return await http.get<Drink[]>(`${baseUrl}/drinks`);
});

export const updatePizza = createAsyncThunk(`${baseUrl}/updatePizza`, async (model: Pizza) => {
  return http.post<Pizza>(`${baseUrl}/pizzas`, model);
});

export const updateDrink = createAsyncThunk(`${baseUrl}/updateDrink`, async (model: Drink) => {
  return http.post<Drink>(`${baseUrl}/drinks`, model);
});

export const deletePizzaById = createAsyncThunk(`${baseUrl}/deletePizzaById`, async (idd: string) => {
  return await http.delete<Product>(`${baseUrl}/${idd}`);
});

export const deleteDrinkById = createAsyncThunk(`${baseUrl}/deleteDrinkById`, async (id: string) => {
  return await http.delete<Product>(`${baseUrl}/${id}`);
});

export const slice = createSlice({
  name: baseUrl,
  initialState,
  reducers: {
    resetPizzas: (state) => {
      state.pizzas = [];
    },
    resetDrinks: (state) => {
      state.drinks = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllPizzas.fulfilled, (state, action) => {
        state.pizzas = action.payload;
      })
      .addCase(getAllDrinks.fulfilled, (state, action) => {
        state.drinks = action.payload;
      })
      .addCase(updatePizza.fulfilled, (state, action) => {
        const pizza = action.payload;
        const index = state.pizzas.findIndex((x) => x.id === pizza.id) ?? -1;
        if (index > -1) {
          state.pizzas[index] = pizza;
        }
      })
      .addCase(updateDrink.fulfilled, (state, action) => {
        const drink = action.payload;
        const index = state.drinks.findIndex((x) => x.id === drink.id) ?? -1;
        if (index > -1) {
          state.drinks[index] = drink;
        }
      })
      .addCase(deletePizzaById.fulfilled, (state, action) => {
        const id = action.payload.id;
        const index = state.pizzas.findIndex((x) => x.id === id) ?? -1;
        if (index > -1) {
          state.pizzas.splice(index, 1);
        }
      })
      .addCase(deleteDrinkById.fulfilled, (state, action) => {
        const id = action.payload.id;
        const index = state.drinks.findIndex((x) => x.id === id) ?? -1;
        if (index > -1) {
          state.drinks.splice(index, 1);
        }
      });
  },
});

export const { resetPizzas, resetDrinks } = slice.actions;
