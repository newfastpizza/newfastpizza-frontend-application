export interface Product {
  id: string;
  name: string;
  description: string;
  price: number;
  image: string;
}

export interface Pizza extends Product {
  size: string;
}

export interface Drink extends Product {
  volume: number;
}
