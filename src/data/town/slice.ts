import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { http } from '../../core/http';
import { TownPoint } from './models';

const baseUrl = 'town';

interface TownPointState {
  points: TownPoint[];
}

const initialState: TownPointState = {
  points: [],
};

export const getAllTownPoints = createAsyncThunk(`${baseUrl}/getAllTownPoints`, async () => {
  const points = await http.get<TownPoint[]>(baseUrl);
  return points.sort((a, b) => {
    const res = a.street.localeCompare(b.street);
    if (res === 0) {
      return a.building.localeCompare(b.building);
    }
    return res;
  });
});

export const setOfficeStatus = createAsyncThunk(
  `${baseUrl}/setOfficeStatus`,
  async ({ id, value }: { id: string; value: boolean }) => {
    return http.post<TownPoint>(`${baseUrl}/${id}`, value);
  }
);

export const slice = createSlice({
  name: baseUrl,
  initialState,
  reducers: {
    resetTownPoints: (state) => {
      state.points = [];
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getAllTownPoints.fulfilled, (state, action) => {
        state.points = action.payload;
      })
      .addCase(setOfficeStatus.fulfilled, (state, action) => {
        const point = action.payload;
        const index = state.points.findIndex((x) => x.id === point.id) ?? -1;
        if (index > -1) {
          state.points[index] = point;
        }
      });
  },
});

export const { resetTownPoints } = slice.actions;
