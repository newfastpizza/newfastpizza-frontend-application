export interface TownPoint {
  id: string;
  street: string;
  building: string;
  isBranchOffice: boolean;
}
