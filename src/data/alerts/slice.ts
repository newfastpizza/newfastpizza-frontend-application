import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { counter } from '../../core/counter';
import { RejectedAction } from '../../core/loader';
import { AlertInfo } from './models';

interface AlertsState {
  items: AlertInfo[];
}

const initialState: AlertsState = {
  items: [],
};

export const slice = createSlice({
  name: 'alerts',
  initialState,
  reducers: {
    showAlert: {
      reducer: (state, action: PayloadAction<AlertInfo>) => {
        state.items.push(action.payload);
      },
      prepare: ({ message, type = 'success' }: Omit<AlertInfo, 'id'>) => {
        return { payload: { id: counter.next.toString(), message, type } };
      },
    },
    closeAlert: (state, action: PayloadAction<string>) => {
      const index = state.items.findIndex((x) => x.id === action.payload);
      if (index > -1) {
        state.items.splice(index, 1);
      }
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      (action): action is RejectedAction => action.type.endsWith('/rejected'),
      (state, action) => {
        state.items.push({
          id: action.meta.requestId,
          message: action.error.message ?? '',
          type: 'danger',
        });
      }
    );
  },
});

export const { showAlert, closeAlert } = slice.actions;
