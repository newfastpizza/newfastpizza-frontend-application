export interface AlertInfo {
  id: string;
  message: string;
  type?: AlertType;
}

export type AlertType = 'success' | 'danger' | 'warning';
