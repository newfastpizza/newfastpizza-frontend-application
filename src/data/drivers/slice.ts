import { createAsyncThunk, createSlice, isAnyOf } from '@reduxjs/toolkit';

import { http } from '../../core/http';
import { Order } from '../orders/models';
import { TownPoint } from '../town/models';
import { ChangeDriverProfileRequest, Driver } from './models';

const baseUrl = 'drivers';

interface DriversState {
  items?: Driver[];
  item: {
    driver?: Driver;
    order?: Order;
    position?: TownPoint;
  };
}

const initialState: DriversState = {
  item: {},
};

export const getDrivers = createAsyncThunk(`${baseUrl}/getDrivers`, async () => {
  return await http.get<Driver[]>(`${baseUrl}`);
});

export const getDriverById = createAsyncThunk(`${baseUrl}/getDriverById`, async (id: string) => {
  return await http.get<Driver>(`${baseUrl}/${id}`);
});

export const updateDriverInfo = createAsyncThunk(
  `${baseUrl}/updateDriverInfo`,
  async ({ id, model }: { id: string; model: ChangeDriverProfileRequest }) => {
    return await http.post<Driver>(`${baseUrl}/${id}`, model);
  }
);

export const deleteDriver = createAsyncThunk(`${baseUrl}/deleteDriver`, async (id: string) => {
  return await http.delete<Driver>(`${baseUrl}/${id}`);
});

export const getOrderForDriverByid = createAsyncThunk(`${baseUrl}/getOrderForDriverByid`, async (id: string) => {
  const response = await http.request(`${baseUrl}/${id}/order`, 'GET');
  try {
    return await http.processJsonResponse<Order>(response);
  } catch {
    return undefined;
  }
});

export const getPositionForDriverById = createAsyncThunk(`${baseUrl}/getPositionForDriverById`, async (id: string) => {
  const response = await http.request(`${baseUrl}/${id}/position`, 'GET');
  try {
    return await http.processJsonResponse<TownPoint>(response);
  } catch {
    return undefined;
  }
});

export const slice = createSlice({
  name: baseUrl,
  initialState,
  reducers: {
    resetDrivers: (state) => {
      state.items = undefined;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getDrivers.fulfilled, (state, action) => {
        state.items = action.payload;
      })
      .addCase(deleteDriver.fulfilled, (state, action) => {
        const driver = action.payload;
        const index = state.items?.findIndex((x) => x.id === driver.id) ?? -1;

        if (state.items && index > -1) {
          state.items.splice(index, 1);
        }
      })
      .addCase(getOrderForDriverByid.fulfilled, (state, action) => {
        state.item.order = action.payload;
      })
      .addCase(getPositionForDriverById.fulfilled, (state, action) => {
        state.item.position = action.payload;
      })
      .addMatcher(isAnyOf(getDriverById.fulfilled, updateDriverInfo.fulfilled), (state, action) => {
        const driver = action.payload;
        const index = state.items?.findIndex((x) => x.id === driver.id) ?? -1;

        if (state.items && index > -1) {
          state.items[index] = driver;
        }

        state.item.driver = driver;
      });
  },
});

export const { resetDrivers } = slice.actions;
