import { Role } from '../clients/models';

export interface Driver {
  id: string;
  username: string;
  name: string;
  phone: string;
  role: Role;
  status: string;
}

export interface ChangeDriverProfileRequest {
  name: string;
  phone: string;
}
