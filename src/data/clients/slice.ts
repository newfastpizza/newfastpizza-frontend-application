import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

import { http } from '../../core/http';
import { ChangeClientProfileRequest, Client } from './models';

const baseUrl = 'clients';

interface ClientsState {
  ownProfile?: Client;
  items?: Client[];
  item?: Client;
}

const initialState: ClientsState = {};

export const getOwnProfile = createAsyncThunk(`${baseUrl}/getOwnProfile`, async () => {
  return await http.get<Client>(`${baseUrl}/profile`);
});

export const updateClientInfo = createAsyncThunk(
  `${baseUrl}/updateClientInfo`,
  async (model: ChangeClientProfileRequest) => {
    return await http.post<Client>(`${baseUrl}/profile`, model);
  }
);

export const deleteClient = createAsyncThunk(`${baseUrl}/deleteClient`, async () => {
  await http.request(`${baseUrl}/profile`, 'DELETE');
});

export const getClients = createAsyncThunk(`${baseUrl}/getClients`, async () => {
  return await http.get<Client[]>(`${baseUrl}`);
});

export const getClientById = createAsyncThunk(`${baseUrl}/getClientById`, async (id: string) => {
  return await http.get<Client>(`${baseUrl}/${id}`);
});

export const slice = createSlice({
  name: baseUrl,
  initialState,
  reducers: {
    resetOwnProfile: (state) => {
      state.ownProfile = undefined;
    },
    resetClients: (state) => {
      state.items = undefined;
    },
  },
  extraReducers: (builder) => {
    builder
      .addCase(getOwnProfile.fulfilled, (state, action) => {
        state.ownProfile = action.payload;
      })
      .addCase(updateClientInfo.fulfilled, (state, action) => {
        state.ownProfile = action.payload;
      })
      .addCase(getClients.fulfilled, (state, action) => {
        state.items = action.payload;
      })
      .addCase(getClientById.fulfilled, (state, action) => {
        state.item = action.payload;
      });
  },
});

export const { resetOwnProfile, resetClients } = slice.actions;
