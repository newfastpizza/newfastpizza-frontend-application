export interface Client {
  id: string;
  username: string;
  name: string;
  email: string;
  phone: string;
  birthday: string | null;
  role: Role;
}

export interface Role {
  roleType: string;
}

export interface ChangePasswordRequest {
  oldPassword: string;
  password: string;
  confirmPassword: string;
}

export interface ChangeClientProfileRequest {
  name: string;
  email: string;
  phone: string;
}
