import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import { createBrowserHistory } from 'history';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';

import App from './app';
import { AlertContainer } from './common/parts/alertContainer';
import { store } from './core/store';

const history = createBrowserHistory();

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Router history={history}>
        <App />
        <AlertContainer />
      </Router>
    </Provider>
  </React.StrictMode>,
  document.getElementById('root')
);
