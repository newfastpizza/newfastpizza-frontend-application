import './alertContainer.css';

import React, { useCallback } from 'react';
import { useSelector } from 'react-redux';

import { RootState, useAppDispatch } from '../../core/store';
import { closeAlert } from '../../data/alerts/slice';
import { Alert } from './alert';

export const AlertContainer: React.FC = () => {
  const dispatch = useAppDispatch();
  const alerts = useSelector((state: RootState) => state.alerts.items);
  const onClose = useCallback((id: string) => dispatch(closeAlert(id)), [dispatch]);

  return (
    <div className="alert-container">
      {alerts.map((a) => (
        <Alert key={a.id} alert={a} onClose={onClose} />
      ))}
    </div>
  );
};
