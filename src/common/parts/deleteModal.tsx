import { AsyncThunk } from '@reduxjs/toolkit';
import React from 'react';
import { Button, Modal } from 'react-bootstrap';

import { LoadingButton } from './loadingButton';

interface Props {
  title?: string;
  asyncThunk: AsyncThunk<any, any, any>;
  onHide: () => void;
  onDelete: () => void;
}

export const DeleteModal: React.FC<Props> = ({ title = '', asyncThunk, onHide, onDelete }) => {
  return (
    <Modal show onHide={onHide} animation={false} centered size="sm">
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body> Are you sure?</Modal.Body>
      <Modal.Footer>
        <Button onClick={onHide}>Close</Button>
        <LoadingButton asyncThunks={[asyncThunk]} onClick={onDelete} variant="danger">
          Delete
        </LoadingButton>
      </Modal.Footer>
    </Modal>
  );
};
