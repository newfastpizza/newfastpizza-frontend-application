import { AsyncThunk } from '@reduxjs/toolkit';
import React from 'react';
import { Spinner } from 'react-bootstrap';

import { useLoaders } from '../../core/customHooks/useLoader';

interface Props {
  asyncThunks: AsyncThunk<any, any, any>[];
}

export const LoadingPanel: React.FC<Props> = ({ asyncThunks, children }) => {
  const items = useLoaders(asyncThunks);

  if (items.some((x) => x.status === 'loading')) {
    return (
      <div className="d-flex justify-content-center">
        <Spinner className="mt-5 mb-5" animation="border" role="status" as="span" aria-hidden="true" />
      </div>
    );
  }

  return <>{children}</>;
};
