import { AsyncThunk } from '@reduxjs/toolkit';
import React from 'react';
import { Button, ButtonProps, Spinner } from 'react-bootstrap';

import { useLoaders } from '../../core/customHooks/useLoader';

interface Props extends ButtonProps {
  asyncThunks: AsyncThunk<any, any, any>[];
}

export const LoadingButton: React.FC<Props> = ({ asyncThunks, children, disabled, ...other }) => {
  const items = useLoaders(asyncThunks);

  const isLoading = items.some((x) => x.status === 'loading');
  const content = isLoading ? (
    <Spinner animation="border" role="status" as="span" size="sm" aria-hidden="true" />
  ) : (
    children
  );

  return (
    <Button disabled={isLoading || disabled} {...other}>
      {content}
    </Button>
  );
};
