import './alert.css';

import classNames from 'classnames';
import React, { useEffect, useState } from 'react';

import { AlertInfo } from '../../data/alerts/models';

interface Props {
  alert: AlertInfo;
  onClose: (id: string) => void;
  closeTime?: number;
  hideDuration?: number;
}

export const Alert: React.FC<Props> = React.memo(({ alert, onClose, closeTime = 10000, hideDuration = 5000 }) => {
  const [hide, setHide] = useState<boolean>();

  useEffect(() => {
    const timeoutId1 = window.setTimeout(() => setHide(true), closeTime - hideDuration);
    const timeoutId2 = window.setTimeout(() => onClose(alert.id), closeTime);
    return () => {
      clearTimeout(timeoutId2);
      clearTimeout(timeoutId1);
    };
  }, [alert.id, hideDuration, onClose, closeTime]);

  return (
    <div
      role="alert"
      className={classNames('alert alert-dismissible', `alert-${alert.type ?? 'danger'}`, { hide }, {})}
      style={{ transition: `opacity ${hideDuration}ms` }}>
      {alert.message}
      <button className="close" onClick={() => onClose(alert.id)}>
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
  );
});
