import React, { useCallback } from 'react';
import { Form } from 'react-bootstrap';

interface Props {
  controlId: string;
  value: string | undefined;
  onChange?: (value: string) => void;
  disabled?: boolean;
}

export const DateField: React.FC<Props> = ({ controlId, value, onChange, disabled, children }) => {
  const onChangeMemoized = useCallback((e: React.ChangeEvent<HTMLInputElement>) => onChange?.(e.target.value), [
    onChange,
  ]);

  return (
    <Form.Group controlId={controlId}>
      {children && <Form.Label>{children}</Form.Label>}
      <Form.Control type="date" value={value} onChange={onChangeMemoized} disabled={disabled} />
    </Form.Group>
  );
};
