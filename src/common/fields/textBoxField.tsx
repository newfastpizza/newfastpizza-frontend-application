import React, { useCallback, useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';
import { reach } from 'yup';

import { isEmpty } from '../../utils/empty';
import { ValidateComponentProps } from './interfaces';

interface Props extends ValidateComponentProps {
  controlId: string;
  value?: string | number | null;
  onChange?: (value: string) => void;
  type?: 'text' | 'number' | 'password';
  disabled?: boolean;
}

export const TextBoxField: React.FC<Props> = ({
  controlId,
  value,
  onChange,
  type = 'text',
  disabled,
  validate,
  schema,
  fieldPath,
  children,
}) => {
  value = value == null ? '' : value;

  const [feedback, setFeedback] = useState(null);

  useEffect(() => {
    let canceled = false;
    if (validate && schema && fieldPath) {
      reach(schema, fieldPath, undefined, undefined)
        .validate(value)
        .then(() => {
          if (!canceled) setFeedback(null);
          return null;
        })
        .catch((x: { message: React.SetStateAction<null> }) => {
          if (!canceled) setFeedback(x.message);
        });
    }
    return () => {
      canceled = true;
    };
  });

  const onChangeMemoized = useCallback((e: React.ChangeEvent<HTMLInputElement>) => onChange?.(e.target.value), [
    onChange,
  ]);

  return (
    <Form.Group controlId={controlId}>
      {children && <Form.Label>{children}</Form.Label>}
      <Form.Control
        type={type}
        value={value}
        onChange={onChangeMemoized}
        disabled={disabled}
        isInvalid={!isEmpty(feedback)}
      />
      {feedback && <Form.Control.Feedback type="invalid">{feedback}</Form.Control.Feedback>}
    </Form.Group>
  );
};
