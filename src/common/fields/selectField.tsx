import React, { useCallback, useEffect, useState } from 'react';
import { Form } from 'react-bootstrap';
import { reach } from 'yup';

import { isEmpty } from '../../utils/empty';
import { ValidateComponentProps } from './interfaces';

interface Props<TOption extends object | string | number> extends ValidateComponentProps {
  controlId: string;
  options: TOption[];
  value?: string | number | null;
  getValue: (option: TOption) => string | number;
  getLabel: (option: TOption) => string;
  onChange?: (value: string) => void;
  useZero?: boolean;
  addEmptyOption?: boolean;
  emptyOptionName?: string;
  disabled?: boolean;
  children?: React.ReactNode;
}

export const SelectField = <TOption extends object | string | number>({
  controlId,
  options,
  getValue,
  getLabel,
  value,
  onChange,
  useZero = false,
  addEmptyOption = false,
  emptyOptionName = '',
  disabled,
  validate,
  schema,
  fieldPath,
  children,
}: Props<TOption>) => {
  value = value == null ? '' : value;

  const [feedback, setFeedback] = useState(null);

  useEffect(() => {
    let canceled = false;
    if (validate && schema && fieldPath) {
      reach(schema, fieldPath, undefined, undefined)
        .validate(value)
        .then(() => {
          if (!canceled) setFeedback(null);
          return null;
        })
        .catch((x: { message: React.SetStateAction<null> }) => {
          if (!canceled) setFeedback(x.message);
        });
    }
    return () => {
      canceled = true;
    };
  });

  const onChangeMemoized = useCallback((e: React.ChangeEvent<HTMLSelectElement>) => onChange?.(e.target.value), [
    onChange,
  ]);

  return (
    <Form.Group controlId={controlId}>
      {children && <Form.Label>{children}</Form.Label>}
      <Form.Control
        custom
        as="select"
        value={value}
        onChange={onChangeMemoized}
        disabled={disabled}
        isInvalid={!isEmpty(feedback)}>
        {renderFirstOption(value, useZero, addEmptyOption, emptyOptionName)}
        {options != null &&
          options.map((option) => (
            <option key={getValue(option)} value={getValue(option)}>
              {getLabel(option)}
            </option>
          ))}
      </Form.Control>
      {feedback && <Form.Control.Feedback type="invalid">{feedback}</Form.Control.Feedback>}
    </Form.Group>
  );
};

const renderFirstOption = (
  value: string | number,
  useZero: boolean,
  addEmptyOption: boolean,
  emptyOptionName: string
) => {
  if (addEmptyOption) {
    return <option value="">{emptyOptionName}</option>;
  } else if (useZero) {
    return !value && value !== 0 ? <option value="" disabled /> : undefined;
  } else {
    return value ? undefined : <option value="" disabled />;
  }
};
