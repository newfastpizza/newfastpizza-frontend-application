import { SchemaOf } from 'yup';

export interface ValidateComponentProps {
  validate?: boolean;
  schema?: SchemaOf<any>;
  fieldPath?: string;
}
