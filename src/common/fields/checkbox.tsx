import React, { useCallback } from 'react';
import { Form } from 'react-bootstrap';

interface Props {
  name: string;
  value: boolean;
  onChange: (value: boolean) => void;
}

export const CheckBox: React.FC<Props> = ({ name, value, onChange }) => {
  const onChangeMemoized = useCallback((e: React.ChangeEvent<HTMLInputElement>) => onChange(e.target.checked), [
    onChange,
  ]);
  return <Form.Check id={name} checked={value} onChange={onChangeMemoized} />;
};
