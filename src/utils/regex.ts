export const usernameRegex = /[0-9a-zA-Z_]+/;
export const nameRegex = /[A-Za-z ]+/;
export const phoneRegex = /^((8|\+7)[- ]?)?(\(?\d{3}\)?[- ]?)?[\d\- ]{7,10}$/;
export const passwordRegex = /^[0-9a-zA-Z!@#$%^&*]+/;
