export function areEqualShallow<T>(a: T, b: T) {
  for (var key in a) {
    if (typeof key === 'number' && Number(a[key]) !== Number(b[key])) {
      return false;
    } else if (a[key] !== b[key]) {
      return false;
    }
  }
  return true;
}
