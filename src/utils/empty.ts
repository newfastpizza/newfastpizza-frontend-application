export const isEmpty = (parameter: string | undefined | null) => parameter == null || parameter === '';
