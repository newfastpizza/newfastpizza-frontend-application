import { AsyncThunk, createSlice } from '@reduxjs/toolkit';

import { RequestStatus } from './interfaces';

export type GenericAsyncThunk = AsyncThunk<unknown, unknown, {}>;

export type PendingAction = ReturnType<GenericAsyncThunk['pending']>;
export type RejectedAction = ReturnType<GenericAsyncThunk['rejected']>;
export type FulfilledAction = ReturnType<GenericAsyncThunk['fulfilled']>;

const pendingActionPostfix = '/pending';
const rejectedActionPostfix = '/rejected';
const fulfilledActionPostfix = '/fulfilled';

const getTypePrefix = (type: string, typePostfix: string) => type.substring(0, type.length - typePostfix.length);

const initialState: Record<string, RequestStatus> = {};

export const slice = createSlice({
  name: 'loader',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addMatcher(
        (action): action is PendingAction => action.type.endsWith(pendingActionPostfix),
        (state, action) => {
          state[getTypePrefix(action.type, pendingActionPostfix)] = { status: 'loading' };
        }
      )
      .addMatcher(
        (action): action is RejectedAction => action.type.endsWith('/rejected'),
        (state, action) => {
          state[getTypePrefix(action.type, rejectedActionPostfix)] = { status: 'failed', error: action.error.message };
        }
      )
      .addMatcher(
        (action): action is FulfilledAction => action.type.endsWith('/fulfilled'),
        (state, action) => {
          state[getTypePrefix(action.type, fulfilledActionPostfix)] = { status: 'succeeded' };
        }
      );
  },
});
