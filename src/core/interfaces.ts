export interface RequestStatus {
  status: 'idle' | 'loading' | 'succeeded' | 'failed';
  error?: string;
}
