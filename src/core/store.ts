import { Action, configureStore, ThunkAction } from '@reduxjs/toolkit';
import { useDispatch } from 'react-redux';

import { slice as alertsSlice } from '../data/alerts/slice';
import { slice as authSlice } from '../data/auth/slice';
import { slice as clientsSlice } from '../data/clients/slice';
import { slice as driversSlice } from '../data/drivers/slice';
import { slice as ordersSlice } from '../data/orders/slice';
import { slice as productsSlice } from '../data/products/slice';
import { slice as townSlice } from '../data/town/slice';
import { slice as loaderSlice } from './loader';

export const store = configureStore({
  reducer: {
    alerts: alertsSlice.reducer,
    auth: authSlice.reducer,
    clients: clientsSlice.reducer,
    drivers: driversSlice.reducer,
    products: productsSlice.reducer,
    orders: ordersSlice.reducer,
    town: townSlice.reducer,
    loader: loaderSlice.reducer,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, Action<string>>;
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
