const baseApiUrl = 'http://localhost:8080/api/';

class HttpWrapper {
  private _headers: HeadersInit = {
    'Content-Type': 'application/json',
  };

  private _getHeaders(current?: HeadersInit): HeadersInit {
    const token = localStorage.getItem('token');
    const headers = current ?? this._headers;
    return token ? { Authorization: 'Bearer ' + token, ...headers } : headers;
  }

  public async processJsonResponse<T>(response: Response): Promise<T> {
    try {
      return (await response.json()) as Promise<T>;
    } catch (ex) {
      const err: Error = ex;
      return Promise.reject(err.message);
    }
  }

  public async processTextResponse(response: Response): Promise<string> {
    try {
      return await response.text();
    } catch (ex) {
      const err: Error = ex;
      return Promise.reject(err.message);
    }
  }

  public async request(url: string, method: 'GET' | 'POST' | 'DELETE' = 'GET', data?: any): Promise<Response> {
    const finalConfig: RequestInit = {
      method,
      body: JSON.stringify(data),
      headers: this._getHeaders(),
    };

    try {
      const response = await fetch(baseApiUrl + url, finalConfig);

      if (!response.ok) {
        let errorMessage: string;

        if (response.status === 401 || response.status === 403) {
          const errorJson = (await response.json()) as { message: string };
          errorMessage = errorJson.message;
        } else {
          errorMessage = await response.text();
        }

        throw new Error(errorMessage);
      } else {
        return response;
      }
    } catch (ex) {
      const err: Error = ex;
      console.log(err.message);
      return Promise.reject(err.message);
    }
  }

  public async get<T>(url: string): Promise<T> {
    const response = await this.request(url);
    return this.processJsonResponse<T>(response);
  }

  public async post<T>(url: string, data?: any): Promise<T> {
    const response = await this.request(url, 'POST', data);
    return this.processJsonResponse<T>(response);
  }

  public async delete<T>(url: string, data?: any): Promise<T> {
    const response = await this.request(url, 'DELETE', data);
    return this.processJsonResponse<T>(response);
  }
}

export const http = new HttpWrapper();
