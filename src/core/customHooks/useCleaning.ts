import { ActionCreatorWithoutPayload } from '@reduxjs/toolkit';
import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

export const useCleaning = (actionCreator: ActionCreatorWithoutPayload) => {
  const dispatch = useDispatch();
  useEffect(() => {
    return () => {
      dispatch(actionCreator());
    };
  }, [dispatch, actionCreator]);
};
