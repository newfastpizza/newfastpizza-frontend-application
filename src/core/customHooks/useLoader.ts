import { AsyncThunk } from '@reduxjs/toolkit';
import { useSelector } from 'react-redux';

import { RequestStatus } from '../interfaces';
import { RootState } from '../store';

export const useLoaders = (asyncThunks: AsyncThunk<any, any, any>[]): RequestStatus[] => {
  return useSelector((state: RootState) =>
    asyncThunks.map((x) => state.loader[x.typePrefix]).filter((x) => x !== undefined)
  );
};
